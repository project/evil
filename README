This module's sole purpose is to test the vulnerability described in
http://drupal.org/node/769264

Testing the Drush bootstrap attack vector
=========================================

To test this, install (but don't enable yet) the module in your site's
directory as a regular user (not your aegir user). For example, drop
the module in sites/example.com/modules/evil/

Then try to make the .drush.inc vulnerability run by calling something
like:

cd sites/example.com ; drush help

At this point, the above should yield the following warnings alongside
the regular help:

I'm in /var/www/drupal/sites/example.com/modules/evil/evil.inc, GOTCHA!!
I'm in /var/www/drupal/sites/example.com/modules/evil/evil.drush.inc, GOTCHA!!

A logfile should also be written in /tmp/evil.txt, with a timestamp.

The key problem here is that we're not even bootstrapping drupal but
we're still loading that file.

With the -R flag (available through a patch), the warnings should go
away, as the .drush.inc files will not be loaded.

Testing the Drupal bootstrap attack vector
==========================================

First, enable the module through the web interface or through drush:

cd sites/example.com ; drush en evil

Then call cron or an eval (which bootstraps the whole drupal) with and
without the -R flag. As things stand right now, the vulnerability is
still there and -R has no effect.

cd sites/example.com ; drush eval "print 1;"