<?php

// why bother with hooks? we're executing code right here...

if (!$_evil_backtrace) {
  // don't call this if we were called already
  _evil_doevil(__FILE__);
}

define('_EVIL_LOGFILE', '/tmp/evil.txt');

function _evil_doevil($callback = "unknown callback") {
  global $_evil_backtrace;
  $message = "HAHAHA! THE EVIL MODULE WON AGAIN!!! - I'm in $callback";
  if (function_exists("drush_log")) {
    drush_log($message, 'warning');
  }
  if (function_exists("drupal_set_message")) {
    drupal_set_message($message, 'warning');
  }
  if (function_exists("watchdog") && function_exists("module_implements")) {
    watchdog('evil', $message, WATCHDOG_WARNING);
  }
  print $message;
  if ($file = fopen(_EVIL_LOGFILE, "a")) {
    fwrite($file, date("YmdHis: ") . $message . "\n");
    fclose($file);
  }
  if (!$_evil_backtrace) {
    $_evil_backtrace = array_reduce(debug_backtrace(), '_evil_backtrace_parser');
    register_shutdown_function('_evil_backtrace_output');
  }
}

/**
 * Print one line of backtrace and smush this in the previous stack
 * output
 *
 * Made to reduce a debug_backtrace() into a printable string.
 *
 * Necessary to get a string version of debug_print_backtrace()
 * without output buffering. It's also necessary because we want to
 * not the backtrace not during the exit but exactly when it happened.
 *
 * @see debug_print_backtrace()
 * @see array_reduce()
 * @see _evil_doevil()
 */
function _evil_backtrace_parser($previous, $line) {
  static $frame = 0;
  return $previous . '#' . $frame++ . ' ' . $line['function'] . '() called at ['.basename($line['file']). ':' . $line['line'] ."]\n";
}

/**
 * Output the backtrace recorded
 *
 * This is designed to run on exit.
 *
 * @see _evil_doevil()
 */
function _evil_backtrace_output() {
  global $_evil_backtrace;
  print("The evil module ran and wrote warnings in ". _EVIL_LOGFILE . ", first caller was:\n");
  print($_evil_backtrace);
}
